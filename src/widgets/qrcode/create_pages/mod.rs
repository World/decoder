// SPDX-License-Identifier: GPL-3.0-or-later
mod text_page;
mod wifi_page;

pub use text_page::TextPage;
pub use wifi_page::WiFiPage;
