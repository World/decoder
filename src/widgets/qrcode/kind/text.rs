// SPDX-License-Identifier: GPL-3.0-or-later
use gtk::{glib, subclass::prelude::*};

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/com/belmoussaoui/Decoder/ui/qrcode_kind_text.ui")]
    pub struct QRCodeText {
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QRCodeText {
        const NAME: &'static str = "QRCodeText";
        type Type = super::QRCodeText;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }
    impl ObjectImpl for QRCodeText {}
    impl WidgetImpl for QRCodeText {}
    impl BoxImpl for QRCodeText {}
}

glib::wrapper! {
    pub struct QRCodeText(ObjectSubclass<imp::QRCodeText>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeText {
    pub fn new(content: String) -> Self {
        let widget = glib::Object::new::<QRCodeText>();
        widget.init(content);
        widget
    }

    fn init(&self, content: String) {
        self.imp().label.set_label(&content);
    }
}
