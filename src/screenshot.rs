use ashpd::desktop::screenshot;
use gtk::{gio, glib, prelude::*};

use crate::utils;

pub async fn capture(root: &gtk::Root) -> anyhow::Result<String> {
    let identifier = ashpd::WindowIdentifier::from_native(root).await;
    let response = screenshot::ScreenshotRequest::default()
        .identifier(identifier)
        .interactive(true)
        .modal(true)
        .send()
        .await?
        .response()?;
    let file = gio::File::for_uri(response.uri().as_str());
    let code = utils::scan(&file)?;
    if let Err(err) = file.delete_future(glib::Priority::default()).await {
        tracing::error!("Could not delete file: {err:?}")
    };

    Ok(code)
}
